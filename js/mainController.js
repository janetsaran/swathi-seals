$(document).ready(function() {
    $("#capcha_div").html('<fieldset class="captchaField">\
    <div class="col-12">\
                                <div class="form-floating">\
                                    <input type="text" id="UserCaptchaCode" required class="mt-2 form-control" placeholder="Enter Captcha.."required>\
                                    <label for="subject">Captcha</label>\
                                </div>\
                            </div>\
    <span id="WrongCaptchaError" class="error"></span>\
    <div class="CaptchaWrap"><br>\
      <div id="CaptchaImageCode" class="CaptchaTxtField">\
        <canvas id="CapCode" class="capcode" width="250" height="70"></canvas>\
      </div>\
      <input type="button" class="ReloadBtn" onclick="CreateCaptcha();"> <br>\
    </div>\
</fieldset>');
    CreateCaptcha();

});

function CreateCaptcha() {
    let captcha;
    let alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let first = alphabets[Math.floor(Math.random() * alphabets.length)];
    let second = Math.floor(Math.random() * 10);
    let third = Math.floor(Math.random() * 10);
    let fourth = alphabets[Math.floor(Math.random() * alphabets.length)];
    let fifth = alphabets[Math.floor(Math.random() * alphabets.length)];
    let sixth = Math.floor(Math.random() * 10);
    cd = first + second + third + fourth + fifth + sixth;

    var c = document.getElementById("CapCode"),
        ctx = c.getContext("2d"),
        x = c.width / 2,
        img = new Image();
    img.src = "img/capback.jfif";
    img.onload = function() {
        var pattern = ctx.createPattern(img, "repeat");
        ctx.fillStyle = pattern;
        ctx.fillRect(0, 0, c.width, c.height);
        ctx.font = "37px ALGERIAN";
        ctx.fillStyle = '#212121';
        ctx.textAlign = 'center';
        ctx.setTransform(0.97, -0.13, 0, 1, 1, 23);
        ctx.fillText(cd, x, 35);
    };
}


$('#form_action').submit(function(event){
    event.preventDefault();
    if (CheckCaptcha() == true) {
        this.submit();
        clear_values()
    }
});

function clear_values() {
    $("#name").val('')
    $("#email").val('')
    $("#message").val('')
    $("#subject").val('')
    $("#UserCaptchaCode").val('')
}

function CheckCaptcha() {
    var result = ValidateCaptcha();


    if ($("#UserCaptchaCode").val() == "" || $("#UserCaptchaCode").val() == null || $("#UserCaptchaCode").val() == "undefined") {
        $('#WrongCaptchaError').text('Try Again!!').show();
        $('#UserCaptchaCode').focus();
        return false;
    } else {
        if (result == false) {
            IsAllowed = false;
            $("#UserCaptchaCode").val("")
            $('#WrongCaptchaError').text('Try Again!!').show();
            CreateCaptcha();
            $('#UserCaptchaCode').focus().select();
            return false;
        } else {
            IsAllowed = true;
            $('#WrongCaptchaError').text('').show();
            $('#UserCaptchaCode').val('').attr('Captcha');
            $('#WrongCaptchaError').text('').show();
            return true;

        }
    }
}


function ValidateCaptcha() {
    var str1 = cd;
    var str2 = $('#UserCaptchaCode').val();
    if (str1 == str2) {
        return true;
    } else {
        return false;
    }
}